<?php

/**
 * Network interfaces API controller.
 *
 * @category   apps
 * @package    network
 * @subpackage rest-api
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2018 ClearFoundation
 * @license    http://www.gnu.org/copyleft/lgpl.html GNU Lesser General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/network/
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// B O O T S T R A P
///////////////////////////////////////////////////////////////////////////////

$bootstrap = getenv('CLEAROS_BOOTSTRAP') ? getenv('CLEAROS_BOOTSTRAP') : '/usr/clearos/framework/shared';
require_once $bootstrap . '/bootstrap.php';

///////////////////////////////////////////////////////////////////////////////
// D E P E N D E N C I E S
///////////////////////////////////////////////////////////////////////////////

use \clearos\apps\network\Iface as Iface;
use \clearos\apps\network\Iface_Manager as Iface_Manager;

clearos_load_library('network/Iface');
clearos_load_library('network/Iface_Manager');

///////////////////////////////////////////////////////////////////////////////
// C L A S S
///////////////////////////////////////////////////////////////////////////////

/**
 * Network interfaces API controller.
 *
 * @category   apps
 * @package    network
 * @subpackage rest-api
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2018 ClearFoundation
 * @license    http://www.gnu.org/copyleft/lgpl.html GNU Lesser General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/network/
 */

class Interfaces extends ClearOS_REST_Controller
{
    /**
     * Network interfaces overview.
     *
     * @param string $name interface name
     *
     * @return API response
     */

    function index_get($name)
    {
        try {
            if (is_null($name)) {
                $manager = new Iface_Manager();
                $data = $manager->get_interface_details();
            } else {
                $iface = new Iface($name);
                $data = $iface->get_info();
                $options['data_options']['role'] = $iface->get_supported_roles();
                $options['data_options']['bootproto'] = $iface->get_supported_bootprotos();
            }

            $this->respond_success($data, $options);
        } catch (\Exception $e) {
            $this->exception_handler($e);
        }
    }
}
